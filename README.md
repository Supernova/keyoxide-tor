This is a docker-compose way to run a [Keyoxide](https://keyoxide.org) instance behind TOR

Using:

[Keyoxide](https://codeberg.org/keyoxide/web)

[goldy/tor-hidden-service](https://github.com/cmehay/docker-tor-hidden-service)

[jwilder/nginx-proxy](https://github.com/nginx-proxy/nginx-proxy)

[jrcs/letsencrypt-nginx-proxy-companion](https://github.com/nginx-proxy/docker-letsencrypt-nginx-proxy-companion)

With a few docker skills, you can get your own Keyoxide instance running with both a clearnet website and a [Tor](https://www.torproject.org) address.


### How to use this?


0) Make sure you have a server which is appropriately hardened against [pwnage](https://haveibeenpwned.com/FAQs) and does not have a web server currently running on it.  If you do, you will need to modify these configurations to conform to your setup.  Taken as is, these configuration setup a new nginx reverse proxy.

1) Setup docker and docker-compose on your server.  I recommend the following official guides:

    - [https://docs.docker.com/engine/install/](https://docs.docker.com/engine/install/)

    - [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

2) Decide on a domain or subdomain to use and setup the A and AAAA records in DNS.

3) Use git to clone my project from https://codeberg.org/Supernova/keyoxide-tor.git into your docker folder.

4) Copy the docker-compose.yml.sample file to docker-compose.yml and edit the file to add your website domain details. These are the lines to modify:

      VIRTUAL_HOST: subdomain.domain.tld

      LETSENCRYPT_HOST: subdomain.domain.tld

      LETSENCRYPT_EMAIL: username@domain.tld

      DOMAIN: subdomain.domain.tld

      ONION_URL: http://toraddress.onion

5) Run the rebuild-dockers.sh script which will create all the dockers

6) Profit!
