#!/bin/bash

docker-compose build --pull
docker-compose up -d

#Verify the onion addresses have not changed
proofsv3=$( docker exec -it docker_tor_1 onions | grep keyoxide )

IFS=':'
read -ra array <<< "$proofsv3"
onion=$( echo -e ${array[1]} | tr -d ' ' )
unset IFS

result=$( grep "$onion" "docker-compose.yml" >/dev/null; echo $? )
case $result in
  0)
    ;;
  *)
    echo "*** The Keyoxide ONION_URL needs updating ***";
    echo "New onion url: $onion";
    ;;
esac

